import { readFileSync } from 'fs'

type Action = 'turn on' | 'turn off' | 'toggle'

interface Coordinates {
  x: number
  y: number
}

class MapLight {
  #map: number[][] = []
  #instructions: { action: Action; start: Coordinates; end: Coordinates }[] = []

  constructor() {
    for (let i = 0; i < 1000; i++) {
      this.#map[i] = []
      for (let j = 0; j < 1000; j++) {
        this.#map[i][j] = 0
      }
    }

    readFileSync('data/day6.txt', 'utf-8').split('\n').forEach(line => {
      const [_, action, start, end] = /^(turn on|toggle|turn off) (\d+,\d+) through (\d+,\d+)$/.exec(line)
      const [startX, startY] = start.split(',').map(Number)
      const [endX, endY] = end.split(',').map(Number)

      this.#instructions.push({
        action: action as Action,
        start: { x: startX, y: startY },
        end: { x: endX, y: endY },
      })
    })
  }

  lightsOn = (): number => {
    this.#instructions.forEach(({ action, start, end }) => {
      for (let i = start.x; i <= end.x; i++) {
        for (let j = start.y; j <= end.y; j++) {
          switch (action) {
            case 'turn on':
              this.#map[i][j] = 1
              break
            case 'turn off':
              this.#map[i][j] = 0
              break
            case 'toggle':
              this.#map[i][j] = this.#map[i][j] === 1 ? 0 : 1
              break
          }
        }
      }
    })

    return this.#map.reduce((acc, row) => acc + row.reduce((acc, light) => acc + light, 0), 0)
  }
}

export default (): number => {
  return (new MapLight()).lightsOn()
}