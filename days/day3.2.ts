import fs from 'node:fs'
import path from 'node:path'

type DirectionType = '>' | '<' | '^' | 'v'

interface Coordinate {
  x: number;
  y: number;
}

class Direction {
  private directions: Array<DirectionType>
  private readonly santaCoordinates: Coordinate
  private readonly robotSantaCoordinates: Coordinate
  private readonly coordinatesAlreadyVisited: Set<string>

  constructor(directions: string) {
    this.directions = directions.split('') as Array<DirectionType>
    this.santaCoordinates = { x: 0, y: 0 }
    this.robotSantaCoordinates = { x: 0, y: 0 }
    this.coordinatesAlreadyVisited = new Set([JSON.stringify(this.santaCoordinates)])
  }

  private calculateCoordinate(direction: DirectionType, isSanta: boolean): void {
    switch (direction) {
      case '>':
        isSanta ? this.santaCoordinates.x++ : this.robotSantaCoordinates.x++
        break
      case '<':
        isSanta ? this.santaCoordinates.x-- : this.robotSantaCoordinates.x--
        break
      case '^':
        isSanta ? this.santaCoordinates.y++ : this.robotSantaCoordinates.y++
        break
      case 'v':
        isSanta ? this.santaCoordinates.y-- : this.robotSantaCoordinates.y--
        break
    }
  }

  private addCoordinateToSet(): void {
    this.directions.forEach((direction, index) => {
      const isSanta = index % 2 === 0
      this.calculateCoordinate(direction, isSanta)
      this.coordinatesAlreadyVisited.add(
        isSanta
          ? JSON.stringify(this.santaCoordinates)
          : JSON.stringify(this.robotSantaCoordinates),
      )
    })
  }

  public getResult(): number {
    this.addCoordinateToSet()
    return this.coordinatesAlreadyVisited.size
  }
}

export default (): number => {
  return (new Direction(
    fs.readFileSync(path.join(process.cwd(), '/data/day3.txt'), { encoding: 'utf8' })
      .split('\n')
      .filter((line) => line.length > 0)[0],
  )).getResult()
}

