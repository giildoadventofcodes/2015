import crypto from 'node:crypto'

export default (): number => {
  let smallestNumber = 0
  let hash = ''
  const key = 'ckczppom'

  for (smallestNumber; smallestNumber < 1000000000000; smallestNumber++) {
    hash = crypto.createHash('md5')
                 .update(key + smallestNumber)
                 .digest('hex')

    if (/^00000/.test(hash)) {
      break
    }
  }

  return smallestNumber
}

