import { readFileSync } from 'node:fs'

type Operation = 'AND' | 'OR' | 'LSHIFT' | 'RSHIFT' | 'NOT' | 'SET'

interface Instruction {
  factor?: number;
  input?: number;
  inputWireA?: string;
  inputWireB?: string;
  operation: Operation;
  output: string
}

class Bits {
  #bits: Record<string, number> = {}
  #instructions: Instruction[] = []

  constructor() {
    for (const line of readFileSync('data/day7.txt', { encoding: 'utf8' }).split('\n')) {
      const [instruction, output] = line.split(' -> ')
      if (instruction.includes('AND')) {
        const [inputWireA, inputWireB] = instruction.split(' AND ')
        this.#instructions.push({
          inputWireA,
          inputWireB,
          operation: 'AND',
          output,
        })
        continue
      }

      if (instruction.includes('OR')) {
        const [inputWireA, inputWireB] = instruction.split(' OR ')
        this.#instructions.push({
          inputWireA,
          inputWireB,
          operation: 'OR',
          output,
        })
        continue
      }

      if (instruction.includes('LSHIFT')) {
        const [inputWireA, factor] = instruction.split(' LSHIFT ')
        this.#instructions.push({
          inputWireA,
          factor: parseInt(factor, 10),
          operation: 'LSHIFT',
          output,
        })
        continue
      }

      if (instruction.includes('RSHIFT')) {
        const [inputWireA, factor] = instruction.split(' RSHIFT ')
        this.#instructions.push({
          inputWireA,
          factor: parseInt(factor, 10),
          operation: 'RSHIFT',
          output,
        })
        continue
      }

      if (instruction.includes('NOT')) {
        const [_, inputWireA] = /^NOT ([a-z]+)$/.exec(instruction)
        this.#instructions.push({
          inputWireA,
          operation: 'NOT',
          output,
        })
        continue
      }

      if (output !== 'a') {
        this.#bits[output] = parseInt(instruction, 10)
      } else {
        this.#instructions.push({
          inputWireA: instruction,
          operation: 'SET',
          output,
        })
      }
    }
  }

  readInstructions(): number {
    let security = 10000
    let instructionNb = this.#instructions.length

    while (this.#instructions.length > 0 && security > 0) {
      this.#instructions = this.#instructions.filter((instruction) => {
        if (['AND', 'OR'].includes(instruction.operation)) {
          if (instruction.inputWireA === '1' && instruction.inputWireB in this.#bits) {
            this.#bits[instruction.output] = ({
              'AND': (1 & this.#bits[instruction.inputWireB!]) & 0xFFFF,
              'OR': (1 | this.#bits[instruction.inputWireB!]) & 0xFFFF,
            })[instruction.operation]
            return false
          }

          if (!(instruction.inputWireA in this.#bits) || !(instruction.inputWireB in this.#bits)) {
            return true
          }

          this.#bits[instruction.output] = ({
            'AND': (this.#bits[instruction.inputWireA!] & this.#bits[instruction.inputWireB!]) & 0xFFFF,
            'OR': (this.#bits[instruction.inputWireA!] | this.#bits[instruction.inputWireB!]) & 0xFFFF,
          })[instruction.operation]
          return false
        }

        if (['LSHIFT', 'RSHIFT'].includes(instruction.operation)) {
          if (!(instruction.inputWireA in this.#bits)) {
            return true
          }

          this.#bits[instruction.output] = ({
            'RSHIFT': (this.#bits[instruction.inputWireA!] >> instruction.factor!) & 0xFFFF,
            'LSHIFT': (this.#bits[instruction.inputWireA!] << instruction.factor!) & 0xFFFF,
          })[instruction.operation]
          return false
        }


        if (instruction.operation === 'NOT') {
          if (!(instruction.inputWireA in this.#bits)) {
            return true
          }

          this.#bits[instruction.output] = (~this.#bits[instruction.inputWireA!]) & 0xFFFF
          return false
        }

        if (instruction.operation === 'SET') {
          if (!(instruction.inputWireA in this.#bits)) {
            return true
          }

          this.#bits[instruction.output] = this.#bits[instruction.inputWireA!]
          return false
        }
      })

      if (this.#instructions.length === instructionNb || 'a' in this.#bits) {
        break
      }

      instructionNb = this.#instructions.length
      security--
    }

    return this.#bits['a'] ?? 0
  }
}

export default (): number => {
  return (new Bits()).readInstructions()
}