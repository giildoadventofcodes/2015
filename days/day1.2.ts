import fs from 'node:fs'
import path from 'node:path'

export default (): number => {
  const instructions = fs.readFileSync(path.join(process.cwd(), '/data/day1.txt'), { encoding: 'utf8' })
                         .split('\n')[0]
    .split('')

  let sum = 0
  for (let i = 0; i < instructions.length; i++) {
    if (instructions[i] === '(') {
      sum += 1
    } else if (instructions[i] === ')') {
      sum -= 1
    }

    if (sum === -1) {
      return i + 1
    }
  }

  return sum
}

