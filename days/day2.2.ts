import fs from 'node:fs'
import path from 'node:path'

export default (): number => {
  return fs.readFileSync(path.join(process.cwd(), '/data/day2.txt'), { encoding: 'utf8' })
           .split('\n')
           .filter((line) => line.length > 0)
           .reduce((sum, lineDimensions) => {
             const lwh = lineDimensions.split('x').map((dimension) => parseInt(dimension, 10))

             const dimMin = Math.min(...lwh)
             const dimMin2 = Math.min(...lwh.filter((_, i) => i !== lwh.indexOf(dimMin)))

             const [l, w, h] = lwh

             return sum + dimMin * 2 + dimMin2 * 2 + l * w * h
           }, 0)
}

