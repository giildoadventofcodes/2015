import { readFileSync } from 'node:fs'

class List {
  readonly #lines: string[] = []

  constructor() {
    this.#lines = readFileSync('data/day5.txt', 'utf-8').split('\n')
  }

  checkGoods = (): number => {
    return this.#lines.reduce<number>((acc, line) => {
      return acc + (this.#checkDouble(line) && this.#checkGood(line) ? 1 : 0)
    }, 0)
  }

  #checkDouble(line: string): boolean {
    for (let i = 0; i < line.length - 1; i++) {
      const double = line[i] + line[i + 1]

      if (line.indexOf(double, i + 2) !== -1) return true
    }

    return false
  }

  #checkGood(line: string): boolean {
    for (let i = 0; i < line.length - 2; i++) {
      if (line[i] === line[i + 2]) return true
    }

    return false
  }
}

export default (): number => {
  return (new List()).checkGoods()
}