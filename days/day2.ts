import fs from 'node:fs'
import path from 'node:path'

export default (): number => {
  return fs.readFileSync(path.join(process.cwd(), '/data/day2.txt'), { encoding: 'utf8' })
           .split('\n')
           .filter((line) => line.length > 0)
           .reduce((sum, lineDimensions) => {
             const [l, w, h] = lineDimensions.split('x').map((dimension) => parseInt(dimension, 10))
             const [side1, side2, side3] = [l * w, w * h, h * l]
             return sum + Math.min(side1, side2, side3) + side1 * 2 + side2 * 2 + side3 * 2
           }, 0)
}

