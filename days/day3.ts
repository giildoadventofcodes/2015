import fs from 'node:fs'
import path from 'node:path'

type DirectionType = '>' | '<' | '^' | 'v'

interface Coordinate {
  x: number;
  y: number;
}

class Direction {
  private directions: Array<DirectionType>
  private readonly coordinates: Coordinate
  private readonly coordinatesAlreadyVisited: Set<string>

  constructor(directions: string) {
    this.directions = directions.split('') as Array<DirectionType>
    this.coordinates = { x: 0, y: 0 }
    this.coordinatesAlreadyVisited = new Set()
  }

  private calculateCoordinate(direction: DirectionType): void {
    switch (direction) {
      case '>':
        this.coordinates.x++
        break
      case '<':
        this.coordinates.x--
        break
      case '^':
        this.coordinates.y++
        break
      case 'v':
        this.coordinates.y--
        break
    }
  }

  private addCoordinateToSet(): void {
    this.directions.forEach(direction => {
      this.calculateCoordinate(direction)
      this.coordinatesAlreadyVisited.add(JSON.stringify(this.coordinates))
    })
  }

  public getResult(): number {
    this.addCoordinateToSet()
    return this.coordinatesAlreadyVisited.size
  }
}

export default (): number => {
  return (new Direction(
    fs.readFileSync(path.join(process.cwd(), '/data/day3.txt'), { encoding: 'utf8' })
      .split('\n')
      .filter((line) => line.length > 0)[0],
  )).getResult()
}

