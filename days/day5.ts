import { readFileSync } from 'node:fs'

class List {
  readonly #lines: string[] = []

  constructor() {
    this.#lines = readFileSync('data/day5.txt', 'utf-8').split('\n')
  }

  checkGoods = (): number => {
    return this.#lines.reduce<number>((acc, line) => {
      return acc + (this.#checkVowels(line) && this.#checkDouble(line) && this.#checkBad(line) ? 1 : 0)
    }, 0)
  }

  #checkVowels(line: string): boolean {
    let vowelNb = 0
    for (const char of line.split('')) {
      if (['a', 'e', 'i', 'o', 'u'].includes(char)) {
        vowelNb++

        if (vowelNb === 3) return true
      }
    }

    return false
  }

  #checkDouble(line: string): boolean {
    for (let i = 0; i < line.length - 1; i++) {
      if (line[i] === line[i + 1]) return true
    }

    return false
  }

  #checkBad = (line: string): boolean => !['ab', 'cd', 'pq', 'xy'].some(bad => line.includes(bad))
}

export default (): number => {
  return (new List()).checkGoods()
}