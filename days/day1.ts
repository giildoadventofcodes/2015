import fs from 'node:fs'
import path from 'node:path'

export default (): number => {
  return fs.readFileSync(path.join(process.cwd(), '/data/day1.txt'), { encoding: 'utf8' })
           .split('\n')[0]
    .split('')
    .reduce((sum, instruction) => {
      if (instruction === '(') {
        return sum + 1
      }

      return sum - 1
    }, 0)
}

